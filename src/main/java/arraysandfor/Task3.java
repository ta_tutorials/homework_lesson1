package arraysandfor;

import java.util.Arrays;

// 3) Создайте одномерный массив целых чисел, переверните его.
// Пример: Вход - [1, 5, 10, 50, 3, 5], выход - [5, 3, 50, 10, 5, 1]
public class Task3 {

    private static final int[] array = {1, 5, 10, 100, 1, 3, 4};

    public static void main(String[] args) {
        System.out.println(Arrays.toString(revertArray(array)));
    }

    private static int[] revertArray(int[] array) {
        int[] revertedArray = new int[array.length];
        for (int i = 0; i < array.length; i++) {
            revertedArray[i] = array[array.length - i - 1];
        }
        return revertedArray;
    }
}
