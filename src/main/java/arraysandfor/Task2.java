package arraysandfor;

import java.util.Arrays;

// Создайте одномерный массив целых чисел,
// замените каждый четный по порядку элемент на 1.
//
// Пример: Вход - [1, 5, 10, 50, 3, 5], выход - [1, 1, 10, 1, 3, 1]
public class Task2 {

    private static final int[] array = {1, 5, 10, 100, 1, 3, 4};

    public static void main(String[] args) {
        replaceEvenItems(array);
        System.out.println(Arrays.toString(array));
    }

    private static void replaceEvenItems(int[] array) {
        for (int i = 0; i < array.length; i++) {
            if ((i + 1) % 2 == 0) {
                array[i] = 1;
            }
        }
    }
}
