package arraysandfor;

// Создайте одномерный массив целых чисел,
// найдите и выведите на экран максимальный по значению элемент массива.
//
// Пример: Вход - [1, 5, 10, 50, 3, 5], выход - 50
public class Task1 {

    private static final int[] array = {1, 5, 10, 100, 1, 3, 4};

    public static void main(String[] args) {
        System.out.println(findMaxFromArray(array));
    }

    private static int findMaxFromArray(int[] array) {
        int max = array[0];
        for (int item : array) {
            if (item > max) {
                max = item;
            }
        }
        return max;
    }
}
