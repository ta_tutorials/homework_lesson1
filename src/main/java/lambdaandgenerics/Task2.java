package lambdaandgenerics;

import java.math.BigDecimal;
import java.util.function.Function;

// Сделайте обобщенный метод transform(Function<T, R>),
// который будет принимать функцию трансформации одного класса в другой.
// Внутри метода нужно реализовать обертку try-catch для отлова исключений и внутри нее вызвать метод Function#apply для вызова функции.
// Затем напишите метод main, запускающий java программу
// и в нем вызовете функцию transform для разных преобразований:
//  - Из строки в число
//  - Из числа в строку
//  - Из строки в BigDecimal
public class Task2 {

    public static void main(String[] args) {
        Integer intFromString = transform("123", Integer::valueOf);
        System.out.println(intFromString);
        String stringFromInt = transform(456, String::valueOf);
        System.out.println(stringFromInt);
        BigDecimal bigDecimalFromString = transform("789", BigDecimal::new);
        System.out.println(bigDecimalFromString);
    }

    private static <T, R> R transform(T source, Function<T, R> transformationFunction) {
        try {
            return transformationFunction.apply(source);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
