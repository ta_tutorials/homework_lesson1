package lambdaandgenerics;

// Напишите код для отложенного выполнения функции с использованием лямбда выражения.
// Для реализации "отложенности" рекомендуется использовать метод Thread.sleep(n),
// где n - кол-во милисекунд для ожидания.
// Сделайте main метод, запускающий Java программу
// и передайте в ваш откладывающий метод функцию распечатки в консоль System.out.println().
//
// В качестве аргумента откладывающего метода рекомендуется использовать интерфейсный тип Runnable.
public class Task1 {

    public static void main(String[] args) {
        performCallback(() -> System.out.println("callback launched"));
    }

    private static void performCallback(Runnable runnable) {
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            // do nothing
        }
        runnable.run();
    }
}
