package collectionsandstream;

import collectionsandstream.util.Person;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

// Сгруппируйте пользователей по возрасту.
// На выходе из метода должна вернуться Map<Integer, List<Person>>,
// где каждому возрасту будет соответствовать список людей.
//
// Подсказка: здесь нужно использовать Collectors.grouppingBy(...)
public class Task6 {

    private static final List<Person> persons =
            List.of(new Person(12, "Имя1", "Фамилия1"), new Person(18, "Имя2", "Фамилия2"),
                    new Person(19, "Имя3", "Фамилия3"), new Person(60, "Имя4", "Фамилия4"),
                    new Person(14, "Имя5", "Фамилия5"), new Person(40, "Имя6", "Фамилия6"),
                    new Person(14, "Имя7", "Фамилия7"), new Person(18, "Имя8", "Фамилия8"));

    public static void main(String[] args) {
        System.out.println(groupPersonsByAges());
    }

    private static Map<Integer, List<Person>> groupPersonsByAges() {
        return persons.stream()
                .collect(Collectors.groupingBy(Person::getAge));
    }
}
