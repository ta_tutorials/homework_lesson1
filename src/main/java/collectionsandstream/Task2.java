package collectionsandstream;

import java.util.List;

// Дан список целых чисел, нужно вернуть список из квадратов этих чисел.
// Т.е. дано List[4,1,3,9], на выходе ожидаем List[16,1,9,81]
public class Task2 {

    private static final List<Integer> sourceList = List.of(4, 1, 3, 9);

    public static void main(String[] args) {
        System.out.println(getSquareOfNumbers(sourceList));
    }

    private static List<Integer> getSquareOfNumbers(List<Integer> source) {
        return source.stream()
                .map(number -> number * number)
                .toList();
    }
}
