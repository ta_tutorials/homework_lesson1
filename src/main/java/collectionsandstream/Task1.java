package collectionsandstream;

import java.util.Set;
import java.util.TreeSet;

// Создайте отсортированную коллекцию уникальных целых чисел, заполните ее значениями и выведите на экран.
public class Task1 {

    public static void main(String[] args) {
        Set<Integer> treeSet = new TreeSet<>();
        treeSet.add(5);
        treeSet.add(8);
        treeSet.add(100);
        treeSet.add(2);
        treeSet.add(3);
        treeSet.add(7);
        treeSet.add(11);
        treeSet.add(6);
        treeSet.add(1);

        System.out.println(treeSet);
    }
}
