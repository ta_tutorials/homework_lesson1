package collectionsandstream;

import collectionsandstream.util.Person;

import java.util.List;

// Создайте список людей (Person) и заполните его несколькими объектами.
// Выведите на консоль имена и возраста всех пользователей, возрастом > 18.
// Выходная строка по каждому из пользователей должна выглядеть, как имя_возраст. (Например, Вася_20)
public class Task5 {

    private static final List<Person> persons =
            List.of(new Person(12, "Имя1", "Фамилия1"), new Person(18, "Имя2", "Фамилия2"),
                    new Person(19, "Имя3", "Фамилия3"), new Person(60, "Имя4", "Фамилия4"),
                    new Person(14, "Имя5", "Фамилия5"), new Person(40, "Имя6", "Фамилия6"));


    public static void main(String[] args) {
        System.out.println(getPersonsDataWithAgesGreaterThan(18));
    }

    private static List<String> getPersonsDataWithAgesGreaterThan(int minAge) {
        return persons.stream()
                .filter(person -> person.getAge() > minAge)
                .map(person -> person.getName() + "_" + person.getAge())
                .toList();
    }
}
